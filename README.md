# HitFilm Exporter for Blender

The **HitFilm Exporter for Blender** addon allows you to export camera data and object position from Blender to HitFilm.

## Addon Installation and Uninstallation
These instructions will detail how to install or uninstall the addon.

### Installing the Addon
Only one version of the addon should be installed at a time. If you are updating the addon to a new version, the previous version must be first uninstalled.

The easiest way to install the HitFilm Exporter for Blender addon is to do so through directly through Blender:

1.  Open the Blender User Preferences menu and select the Add-ons tab (**File > User Preferences > Add-ons**)
2.  Click the Install Add-on from File... button at the bottom of the Add-ons menu. This will open a file menu where you can select the **io_export_hitfilm.py** for Blender addon file
3. After installing the addon file, Blender will filter the addons list to only show the **HitFilm Exporter for Blender** addon. Click the checkbox next to the title to **enable the addon**

### Uninstalling the Addon
Only one version of the addon should be installed at a time. If you are updating the addon to a new version, the previous version must be first uninstalled. The easiest way to uninstall the addon is to do it directly through Blender:

1.  **Restart Blender**. To ensure that the addon uninstalls correctly and without errors, Blender should be restarted after using the addon.
2.  Open the Blender User Preferences menu and select the Add-ons tab (**File > User Preferences > Add-ons**)
Search for the **HitFilm Exporter for Blender** addon and click the checkbox next to the addon title to disable the addon
3.  Click the Remove button to remove the addon from Blender

*Note: If you are updating the addon to a new version, Blender should be restarted after uninstalling the previous version. This will ensure that all loaded scripts from the previous version are properly unloaded and do not cause any conflicts with the new version.*

### Updating the Addon
Before updating to a new version, you must first uninstall the previous version. The safest way to update to a new version is to:

Restart Blender
Uninstall previous version
Restart Blender
Install new version
Restart Blender
